"""
Class of the field constraints widget
"""

# pylint:disable=too-many-instance-attributes

from pathlib import Path
from typing import Any, Optional

import yaml
from qgis.gui import QgsFileWidget
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import (
    QComboBox,
    QDialog,
    QDialogButtonBox,
    QDoubleSpinBox,
    QGridLayout,
    QGroupBox,
    QHeaderView,
    QLabel,
    QMessageBox,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QVBoxLayout,
)

from QompliGIS.utils import check_conf_dict_format


class FieldConstraintDialog(QDialog):
    """
    Class of the field constraints widget
    """

    def __init__(self, parent):
        super().__init__(parent)
        self.type_lbl = QLabel(self.tr("Constraint's type"))

        self.type_cbx = QComboBox(self)
        self.type_cbx.addItem(self.tr("Intervals"), "intervals")
        self.type_cbx.addItem(self.tr("RegEx"), "regex")
        self.type_cbx.addItem(self.tr("Relational"), "relational")

        upload_file = QgsFileWidget()
        upload_file.setFilter(self.tr("YAML file") + " (*.yaml *.yml)")
        upload_file.fileChanged.connect(self.config_table)

        new_constraint_btn = QPushButton(self.tr("Add the constraint to the list"))
        new_constraint_btn.clicked.connect(self.value_input)
        new_constraint_btn.setObjectName("new_constraint_btn")

        self.init_interval()
        self.init_regex()
        self.init_relational()
        self.init_summary()

        self.setWindowTitle(self.tr("Constraints Window"))
        self.resize(600, 300)
        self.validate_btn = QDialogButtonBox.Ok
        self.exit_btn = QDialogButtonBox.Cancel
        dlg_btn = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        dlg_btn.accepted.connect(self.accept)
        dlg_btn.rejected.connect(self.reject)

        # Main layout for the popup dialog & add constraints type
        main_layout = QVBoxLayout()
        self.constraint_type()

        # Create a group box & sub-layout within the group layout
        load_constraint_groupbox = QGroupBox(
            self.tr("Load constraints from an existing configuration"), self
        )
        load_constraint_layout = QGridLayout()
        load_constraint_groupbox.setLayout(load_constraint_layout)
        load_constraint_layout.addWidget(upload_file, 0, 0)
        # Add YAML load section to the group layout
        self.config_load(load_constraint_layout)

        # Add relational constraint section to the sub layout
        self.type_cbx.currentIndexChanged.connect(self.constraint_type)
        self.table_cbx_relational.currentIndexChanged.connect(self.input_relational)

        # Create a group box for the "new constraint" widgets
        new_constraint_layout = QGridLayout()
        new_constraint_groupbox = QGroupBox(self.tr("Create a new constraint"), self)
        new_constraint_groupbox.setLayout(new_constraint_layout)
        new_constraint_layout.addWidget(self.type_lbl, 0, 0)
        new_constraint_layout.addWidget(self.type_cbx, 0, 1)
        self.setup_interval(new_constraint_layout)
        self.setup_regex(new_constraint_layout)
        self.setup_relational(new_constraint_layout)
        new_constraint_layout.addWidget(new_constraint_btn, 3, 0, 1, 2)

        # Create a group box for the final constraints list
        summary_groupbox = QGroupBox(self.tr("Constraints summary"))
        summary_groupbox.setLayout(QVBoxLayout())
        summary_groupbox.layout().addWidget(self.summary_wdg)

        main_layout.addWidget(load_constraint_groupbox)
        main_layout.addWidget(new_constraint_groupbox)
        main_layout.addWidget(summary_groupbox)
        main_layout.addWidget(dlg_btn)

        self.setLayout(main_layout)

    def delete_row(self):
        """
        Deletes the selected row from the summary_wdg
        """

        selected_row = self.summary_wdg.currentRow()
        self.summary_wdg.removeRow(selected_row)

    def init_relational(self):
        """
        Initializes QComboBox for relational constraints.
        """
        self.table_lbl_relational = QLabel(self.tr("Table"), self)
        self.field_lbl_relational = QLabel(self.tr("Fields"), self)

        # Labels and combo boxes for table and fields selection
        self.field_cbx_relational = QComboBox(self)
        self.table_cbx_relational = QComboBox(self)

    def init_interval(self):
        """
        Initializes QComboBox for interval constraints.
        """
        # Labels and combo boxes for value and type selection
        self.type_lbl_interval = QLabel(self.tr("Type"))
        self.value_lbl_interval = QLabel(self.tr("Value"))
        self.type_cbx_interval = QComboBox()
        self.type_cbx_interval.addItem(self.tr("Min"), "min")
        self.type_cbx_interval.addItem(self.tr("Max"), "max")
        self.value_spb_interval = QDoubleSpinBox()
        self.value_spb_interval.setRange(float("-inf"), float("+inf"))

    def init_regex(self):
        """
        Initializes QComboBox for regex constraints
        """

        self.value_cbxqle_regex = QComboBox()
        self.value_cbxqle_regex.setEditable(True)
        self.value_cbxqle_regex.addItems([r"[0-9]", "[a-z]", r"^\d{4}-\d{2}-\d{2}$"])

    def setup_relational(self, sub_layout: QGridLayout):
        """
        Add relational constraints in the sub-layout

        Args:
            sub_layout: QGridLayout
            layer_names: list
        """
        sub_layout.addWidget(self.table_lbl_relational, 1, 0)
        sub_layout.addWidget(self.table_cbx_relational, 1, 1)
        sub_layout.addWidget(self.field_lbl_relational, 2, 0)
        sub_layout.addWidget(self.field_cbx_relational, 2, 1)

        # Labels and combo boxes for table and fields selection
        layer_names = self.layers_list()
        self.table_cbx_relational.addItems(layer_names)
        fields_name = self.fields_list(layer_names[0])
        self.field_cbx_relational.addItems(fields_name)

    def input_relational(self):
        """
        Updates field_cbx_relational.
        Connected to signal table_cbx_relational
        """

        self.field_cbx_relational.clear()
        layer_selected = self.table_cbx_relational.currentText()
        fields_name = self.fields_list(layer_selected)
        self.field_cbx_relational.addItems(fields_name)

    def setup_interval(self, sub_layout: QGridLayout):
        """
        Add interval constraints in the sub-layout

        Args:
            sub_layout: QGridLayout
        """
        sub_layout.addWidget(self.type_lbl_interval, 1, 0)
        sub_layout.addWidget(self.type_cbx_interval, 1, 1)
        sub_layout.addWidget(self.value_lbl_interval, 2, 0)
        sub_layout.addWidget(self.value_spb_interval, 2, 1)

    def setup_regex(self, sub_layout: QGridLayout):
        """
        Add regex constraints in the sub-layout

        Args:
            sub_layout: QGridLayout
        """
        sub_layout.addWidget(self.value_cbxqle_regex, 1, 1)

    def init_summary(self):
        """
        Initializes the summary section: summary_wdg.

        Args: None
        """
        self.summary_wdg = QTableWidget()
        self.summary_wdg.setColumnCount(3)
        self.summary_wdg.setRowCount(0)
        self.summary_wdg.setHorizontalHeaderLabels(
            [self.tr("Type"), self.tr("Value"), self.tr("Delete")]
        )
        self.summary_wdg.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.summary_wdg.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)

    def constraint_type(self):
        """
        Label and combo box for selecting constraint type
        """
        index = self.type_cbx.currentData()
        widgets_to_show = []
        all_widgets = [
            self.type_lbl_interval,
            self.value_lbl_interval,
            self.type_cbx_interval,
            self.value_spb_interval,
            self.table_lbl_relational,
            self.field_lbl_relational,
            self.field_cbx_relational,
            self.table_cbx_relational,
            self.value_cbxqle_regex,
        ]

        if index == "relational":
            widgets_to_show = [
                self.table_lbl_relational,
                self.field_lbl_relational,
                self.field_cbx_relational,
                self.table_cbx_relational,
            ]
        elif index == "intervals":
            widgets_to_show = [
                self.type_lbl_interval,
                self.value_lbl_interval,
                self.type_cbx_interval,
                self.value_spb_interval,
            ]
        elif index == "regex":
            widgets_to_show = [self.value_cbxqle_regex]
        for widget in all_widgets:
            widget.setVisible(widget in widgets_to_show)

    def layers_list(self):
        """
        Returns:
            list for the relational constraint tables name
        """

        return [
            table_name
            for table_name in self.parent().conf_dict.keys()
            if table_name not in ["filepath", "input_format", "constraints"]
        ]

    def value_input(self):
        """
        Add a constraint in summary table
        """
        index = self.type_cbx.currentData()
        if index == "regex":
            value = self.value_cbxqle_regex.currentText()
            type_constraints = index

        elif index == "relational":
            value = {
                "table": self.table_cbx_relational.currentText(),
                "field": self.field_cbx_relational.currentText(),
            }
            type_constraints = index

        elif index == "intervals":
            type_constraints = self.type_cbx_interval.currentData()
            value = self.value_spb_interval.value()

        else:
            return

        self.summary_table(value, type_constraints)

    def fields_list(self, layer_name: str):
        """
        Returns the list of fields for layer layer_name
        """
        return list(self.parent().conf_dict[layer_name]["fields"].keys())

    def config_load(self, group_layout: QGridLayout):
        """
        Section for loading existing configuration

        Args:
        group_layout: QGridLayout
        """
        #  widget for constraints from qompligis config
        self.config_wdg = QTableWidget()
        self.config_wdg.setColumnCount(3)
        self.config_wdg.setHorizontalHeaderLabels(
            [self.tr("Type"), self.tr("Value"), self.tr("Load")]
        )
        self.config_wdg.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.config_wdg.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)
        # Add widgets to the layouts
        group_layout.addWidget(self.config_wdg, 1, 0)

    def config_table(self, file_path: str | Path):
        """
        Show the constraints value from the config upload in
        self.config_wdg (QTableWidget)

        Args:
        filepath : str or Path
        """
        if not file_path or not Path(file_path).exists():
            return

        data = None
        with Path(file_path).open("r", encoding="utf8") as file:
            data = yaml.full_load(file)
        if data is None:
            return

        check = check_conf_dict_format(data)
        if check != "":
            QMessageBox.warning(None, self.tr("Bad configuration file!"), check)
            return

        for layer_data in data.values():
            if "fields" not in layer_data:
                continue
            for field_data in layer_data["fields"].values():
                if "constraints" not in field_data:
                    continue
                for constraint_data in field_data["constraints"].values():
                    value_constraint = constraint_data["value"]
                    type_constraint = constraint_data["type"]
                    new_row = self.config_wdg.rowCount()
                    self.config_wdg.insertRow(new_row)
                    item_type = QTableWidgetItem(type_constraint)
                    item_type.setFlags(item_type.flags() & ~Qt.ItemFlag.ItemIsEditable)
                    self.config_wdg.setItem(new_row, 0, item_type)
                    item = QTableWidgetItem(str(value_constraint))
                    item.setData(Qt.ItemDataRole.UserRole, value_constraint)
                    item.setFlags(item.flags() & ~Qt.ItemFlag.ItemIsEditable)
                    self.config_wdg.setItem(new_row, 1, item)
                    btn_upload = QPushButton("")
                    btn_upload.setIcon(
                        QIcon(":images/themes/default/mActionSharingImport.svg")
                    )
                    self.config_wdg.setCellWidget(new_row, 2, btn_upload)
                    btn_upload.clicked.connect(self.load_constraint)

    def load_constraint(self):
        """
        Get the selected row and call self.summary_table() to load it.
        """

        selected_row = self.config_wdg.currentRow()
        type_constraint = self.config_wdg.item(selected_row, 0).text()
        value = self.config_wdg.item(selected_row, 1).data(Qt.ItemDataRole.UserRole)
        self.summary_table(value, type_constraint)

    def summary_table(self, value: Any, type_constraint: str):
        """
        Displaying a summary of constraints

        Args:
        value : str
        type constraint: str
        """

        if value == "":
            return

        row = self.summary_wdg.rowCount()
        self.summary_wdg.insertRow(row)
        btn_delete = QPushButton()
        btn_delete.setIcon(QIcon(":images/themes/default/mActionDeleteSelected.svg"))
        self.summary_wdg.setCellWidget(row, 2, btn_delete)
        btn_delete.clicked.connect(self.delete_row)
        item = QTableWidgetItem(type_constraint)
        item.setFlags(item.flags() & ~Qt.ItemFlag.ItemIsEditable)
        self.summary_wdg.setItem(row, 0, item)
        item = QTableWidgetItem(str(value))
        item.setFlags(item.flags() & ~Qt.ItemFlag.ItemIsEditable)
        item.setData(Qt.ItemDataRole.UserRole, value)
        self.summary_wdg.setItem(row, 1, item)

    def set_summary_table(self, initial_summary: Optional[dict]):
        """
        Set the summary table with the constraints in initial_summary dictionnary
        """
        self.summary_wdg.setRowCount(0)
        if initial_summary is None:
            return

        for constraint in initial_summary.values():
            self.summary_table(constraint["value"], constraint["type"])

    def summary_table_to_dict(self) -> dict:
        """
        Create a dictionnary from the summary constraints table
        """

        constraints_dict = {}
        for row in range(self.summary_wdg.rowCount()):
            constraints_dict[row] = {
                "type": self.summary_wdg.item(row, 0).text(),
                "value": self.summary_wdg.item(row, 1).data(Qt.ItemDataRole.UserRole),
            }

        return constraints_dict
