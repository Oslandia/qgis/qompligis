[general]
name=QompliGIS
about=QGIS compliance plugin
category=Plugins
description=verify if the structure of a dataset complies with the structure of a reference dataset.
icon=resources/images/qompligis_logo.svg
tags=cad, compliance, reporting, quality, standard

# credits and contact
author=Loïc Bartoletti (Oslandia), Jacky Volpes (Oslandia), Vincent Bré (Oslandia)
email=qgis@oslandia.com
homepage=https://oslandia.gitlab.io/qgis/qompligis/
repository=https://gitlab.com/oslandia/QGIS/qompligis/
tracker=https://gitlab.com/Oslandia/qgis/qompligis/-/issues

# experimental flag
deprecated=False
experimental=False
qgisMinimumVersion=3.16
qgisMaximumVersion=3.99
supportsQt6=no

# versionnement
version=1.2.0
changelog=
