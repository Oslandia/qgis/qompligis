# Changelog

## [1.2.0] - 2022/07/20

- Bug fix: the user can now edit his configuration when a field has been added on one of the concerned layers
- When the user edits his configuration, a message about the added/deleted fields is displayed
- The user can export his report in json format (thanks to the students of UGA (University of Grenoble, France) and their profesor Humbert Fiorino )
- Improvement of the report in html format: addition of a navigation bar and css style (thanks to the students of UGA and their profesor)
- Verification of geometry and topology rules : invalid/null/empty geometry and duplicates/overlaps in the same layer (thanks to the students of UGA and their profesor)
  
Thanks to the students of UGA (University of Grenoble, France) and their profesor Humbert Fiorino 
for their contributions to the following functionalities: report in json format, html report style, 
control of geometry and topology rules

## [1.1.0] - 2022/03/22

- Bug fix
- Processing now outputs a boolean for compliance check

## [1.0.3] - 2022/01/17

- Add a window to select a file/folder if the path is not correct.
- Minor code changes
