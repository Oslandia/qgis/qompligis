"""
Test for the class FieldConstraintDialog
"""

from pathlib import Path

from pytestqt.qtbot import QtBot
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QPushButton, QWidget

from QompliGIS.field_constraint_dialog import FieldConstraintDialog


class MockConfigPage(QWidget):
    """Mimmic a ConfigPage with its conf_dict"""

    def __init__(self):
        super().__init__()
        self.conf_dict = {
            "layer1": {
                "fields": {
                    "field1": {"mandatory": False},
                    "field2": {"mandatory": False},
                }
            },
            "layer2": {
                "fields": {
                    "field3": {"mandatory": False},
                    "field4": {"mandatory": False},
                    "field5": {"mandatory": False},
                }
            },
        }


def test_addIntervalsConstraints(qtbot: QtBot):
    """test adding a minimum and maximum constraint"""

    # Create the dialog
    mock_config_page = MockConfigPage()
    fcd = FieldConstraintDialog(parent=mock_config_page)
    add_btn = fcd.findChild(QPushButton, "new_constraint_btn")
    qtbot.addWidget(fcd)

    # Choose constraint minimum
    fcd.type_cbx.setCurrentIndex(0)
    assert fcd.type_cbx.currentData() == "intervals"
    fcd.type_cbx_interval.setCurrentIndex(0)
    assert fcd.type_cbx_interval.currentData() == "min"

    # Choose a minimum value and add the constraint
    fcd.value_spb_interval.setValue(34.6)
    qtbot.mouseClick(add_btn, Qt.MouseButton.LeftButton)

    # Verify that the constraint is correctly set
    assert fcd.summary_table_to_dict() == {0: {"value": 34.6, "type": "min"}}
    assert fcd.summary_wdg.rowCount() == 1
    assert fcd.summary_wdg.item(0, 0).text() == "min"
    assert fcd.summary_wdg.item(0, 1).text() == "34.6"

    # Choose constraint maximum
    fcd.type_cbx_interval.setCurrentIndex(1)
    assert fcd.type_cbx_interval.currentData() == "max"

    # Choose a maximum value and add the constraint
    fcd.value_spb_interval.setValue(103)
    qtbot.mouseClick(add_btn, Qt.MouseButton.LeftButton)

    # Verify that the constraint is correctly set
    assert fcd.summary_table_to_dict() == {
        0: {"value": 34.6, "type": "min"},
        1: {"value": 103, "type": "max"},
    }
    assert fcd.summary_wdg.rowCount() == 2
    assert fcd.summary_wdg.item(1, 0).text() == "max"
    assert fcd.summary_wdg.item(1, 1).text() == "103.0"


def test_addRegexConstraint(qtbot: QtBot):
    """test adding a RegEx constraint"""

    # Create the dialog
    mock_config_page = MockConfigPage()
    fcd = FieldConstraintDialog(parent=mock_config_page)
    add_btn = fcd.findChild(QPushButton, "new_constraint_btn")
    qtbot.addWidget(fcd)

    # Choose constraint regex
    fcd.type_cbx.setCurrentIndex(1)
    assert fcd.type_cbx.currentData() == "regex"
    fcd.value_cbxqle_regex.setEditText(r"qb 5 j;\.+")
    qtbot.mouseClick(add_btn, Qt.MouseButton.LeftButton)

    # Verify that the constraint is correctly set
    assert fcd.summary_table_to_dict() == {
        0: {"value": r"qb 5 j;\.+", "type": "regex"},
    }
    assert fcd.summary_wdg.rowCount() == 1
    assert fcd.summary_wdg.item(0, 0).text() == "regex"
    assert fcd.summary_wdg.item(0, 1).text() == r"qb 5 j;\.+"


def test_addRelationalConstraint(qtbot: QtBot):
    """test adding a relational constraint"""

    # Create the dialog
    mock_config_page = MockConfigPage()
    fcd = FieldConstraintDialog(parent=mock_config_page)
    add_btn = fcd.findChild(QPushButton, "new_constraint_btn")
    qtbot.addWidget(fcd)

    # Choose constraint regex
    fcd.type_cbx.setCurrentIndex(2)
    assert fcd.type_cbx.currentData() == "relational"
    fcd.table_cbx_relational.setCurrentText("layer2")
    fcd.field_cbx_relational.setCurrentText("field3")
    qtbot.mouseClick(add_btn, Qt.MouseButton.LeftButton)

    # Verify that the constraint is correctly set
    assert fcd.summary_table_to_dict() == {
        0: {"value": {"table": "layer2", "field": "field3"}, "type": "relational"},
    }
    assert fcd.summary_wdg.rowCount() == 1
    assert fcd.summary_wdg.item(0, 0).text() == "relational"
    assert fcd.summary_wdg.item(0, 1).text() == "{'table': 'layer2', 'field': 'field3'}"


def test_loadExistingConstraints(qtbot: QtBot, tmp_path):
    """test loading existing constraints from a configuration yaml file"""

    # Create a temporary test yaml config file from the ref test data config file
    test_yaml: Path = tmp_path / "test.yaml"
    test_yaml.write_text(
        (Path(__file__).parent / "testdata" / "test.yaml.ref")
        .read_text()
        .replace("{%filepath%}", "filepath")
    )

    # Create the dialog
    mock_config_page = MockConfigPage()
    fcd = FieldConstraintDialog(parent=mock_config_page)
    qtbot.addWidget(fcd)

    # Load the configuration file
    fcd.config_table(test_yaml)
    assert fcd.config_wdg.rowCount() == 3

    # Add a constraint
    fcd.config_wdg.setCurrentCell(1, 0)
    qtbot.mouseClick(fcd.config_wdg.cellWidget(1, 2), Qt.MouseButton.LeftButton)

    # Verify that the constraint is correctly set
    assert fcd.summary_table_to_dict() == {
        0: {"value": 90, "type": "max"},
    }
    assert fcd.summary_wdg.rowCount() == 1
    assert fcd.summary_wdg.item(0, 0).text() == "max"
    assert fcd.summary_wdg.item(0, 1).text() == "90"
