# Funding

```{admonition} Ready to contribute?
Plugin is free to use, not to develop. If you use it quite intensively or are interested in improving it, please consider to contribute to the code, to the documentation or fund some developments:

- [identified enhancements](https://gitlab.com/Oslandia/qgis/qompligis/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=enhancement)
- want to fund? Please, [send us an email](mailto:qgis@oslandia.com)
```

## Sponsors

Thanks to the sponsors:

```{eval-rst}
.. grid:: 1 2 3 3
    :gutter: 2

    .. grid-item-card::
        :img-top: ../../QompliGIS/resources/images/logo_megeve.jpg
        :class-img-top: sd-px-5
        :shadow: lg

        Megève

        ^^^^^^

        The commune funded the initial development.

        ++++++

        .. button-link:: https://mairie.megeve.fr
            :color: primary
            :outline:
            :align: center
            :expand:

            Website


```
